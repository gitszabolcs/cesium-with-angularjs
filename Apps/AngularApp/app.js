var app = angular.module("cesiumApp",["ngRoute", "appServices", "appControllers", "appDirectives"]);
var appServices = angular.module('appServices', []);
var appControllers = angular.module('appControllers', []);
var appDirectives = angular.module('appDirectives', []);

app.config(["$routeProvider", function($routeProvider){
  $routeProvider.when('/home', {
    templateUrl: "Apps/AngularApp/views/home.html",
    controller: "HomeController"
  }).when('/simple-poligons', {
    templateUrl: "Apps/AngularApp/views/simple_polygons.html",
    controller: "SimplePolygonsController"
  }).otherwise({
            redirectTo: '/home'
  });
}]);

app.run(function($location){
    Cesium.BingMapsApi.defaultKey = "AkBouk0_4wprNGaS9y3hjuAjpLGKuAP2a5bimAtARlbkBfQdXV-zFGtLd-HPgPFg";
});
