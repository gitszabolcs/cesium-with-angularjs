"use strict";
appControllers.controller("NavBarController", ["$scope", "$location", function($scope, $location){
  $scope.redirectTo = function(where){
    $location.path("/"+where);
  };
}]);
