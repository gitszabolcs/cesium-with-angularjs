appControllers.controller("SimplePolygonsController", ["$scope", "$location", function($scope, $location){

  var locals = {};

  $scope.exampleCode = "<div cesium-viewer c-ellipse polygons=\"p\" id=\"cesiumContainer\"></div>";

  $scope.p = [];

  locals.firstEllipse = {
    lat: 44.0,
    long: 26.0,
    semiMinorAxis : 250000.0,
    semiMajorAxis : 400000.0
  };

  $scope.p.push(locals.firstEllipse);

  $scope.addEllipse = function(){
    $scope.p.push(angular.extend({},$scope.newEllipse));
  };
}]);
