appDirectives.directive('cEllipse', ['$rootScope','$timeout', function($rootScope, $timeout){
	return {
		scope: {
      polygons: '=polygons'
    },
		require: 'cesiumViewer',
		link: function($scope, iElm, iAttrs, cesiumViewerCtrl) {
      console.log($scope.polygons);
      var viewer = cesiumViewerCtrl.viewer;
      var entities = [];


      $scope.$watchCollection("polygons", function(newValue, oldValue){
        if(typeof newValue !== "undefined"){
          viewer.entities.removeAll();
          for(var i in newValue){
            entities.push(
              viewer.entities.add({
                position: Cesium.Cartesian3.fromDegrees(newValue[i].lat, newValue[i].long),
                ellipse : {
                  semiMinorAxis : newValue[i].semiMinorAxis,
                  semiMajorAxis : newValue[i].semiMajorAxis,
                  material : Cesium.Color.BLUE.withAlpha(0.5)
                }
              })
            );
            viewer.flyTo(viewer.entities);
            var ellipse = entities[i].ellipse; // For upcoming examples
          }
          console.log(entities);
        }

      });
		}
	};
}]);
