appDirectives.directive('cesiumViewer', ['$rootScope','$timeout', function($rootScope, $timeout){
	return {
		controller: function($scope, $element, $attrs, $transclude) {
			this.viewer = new Cesium.Viewer($attrs.id);
		},
		restrict: 'A',
		link: function($scope, iElm, iAttrs, controller) {
		}
	};
}]);
